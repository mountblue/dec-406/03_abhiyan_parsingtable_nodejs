'use strict';

/**
 * name:        parseFromHTML.js
 * description: A Table parsing  JS script
 * input:       .html file containing the HTML table for parsing
 * output:      Table data in .json format
 * usage:       node parseFromHTML.js <filename>
 *                                     
 * example:     node parseFromHTML.js '1.html' '
 */

//Initializing global variables
const fs =require('fs');
const path = require('path');
const stripTags = require('striptags');
var tableData = [];
const cTable = require('console.table');

//Reading from file 
const readFile = filePath=>fs.readFileSync(filePath,'UTF-8');

//Function to apply regular expresion to parse the HTML
function parser(contents) {
  
  //To find The index of table tag Start
  var index1 = /\<table class="table my-table"\>/.exec(contents).index;
    
  //TO find index of table tag end
  var index2 = /\<\/table\>/.exec(contents).index;
  
  //Parsing contents between table tags
  contents = contents.substring(index1,index2);
  contents = stripTags(contents , ['tr']);
  contents = contents.trim();
  contents = contents.replace(/[\n\t]/g,"");
  contents =contents.split('</tr>');
    
  //Removing spaces and new lines
  contents = contents.slice(0,21);
  contents = contents.map(entry=>entry.trim());
  contents = contents.map(entry=>entry.replace(/\s\s+/g," "));
  return contents;
}
const convertToTable = (tableData)=>{
  return  cTable.getTable(tableData);
}
//Function to convert to JSON format
function convertToJSON(contents) {
  for(let i=0 ; i<contents.length ; i++) {
    //Entries in table    
    if( i !== 0 ) {
     let tuple = contents[i].split(" ");
     tuple = tuple.slice(1);
     let row = {};
     for(let k = 0 ; k<tuple.length ; k++) {
        row[thead[k]] = tuple[k];
     } 
     tableData.push(row);
    }
    //Case for headers
    else {
     let headers = contents[i].split(' ');
     headers = headers.slice(2);
     headers[1] = headers.slice(1,3).join(' ') ;
     headers[2] = headers.slice(3,5).join(' ') ;
     headers.splice(3,2);
     headers[3] = headers.slice(3,5).join(' ');
     headers[4] = headers.slice(5,7).join(' ');
     headers.splice(5,2);
     thead = headers ;
    }
  }
}

//Driver Function 
function main() {
  //Initializing Variables
  const fileName = '1.html';
  const filePath = path.join(__dirname,fileName);
  var thead = [];
  let option = process.argv[2];
  if(!option) {
    console.log(`---------- PLEASE GIVE OPTION 0 for parse and any for Display`);
    process.exit(0);  
  }
  if(option == 1) {
    //Reading file 
    var contents = readFile(filePath);
    
    //Sending contentes to parser function
      contents = parser(contents);
    
      //Converting To JSON format
      convertToJSON(contents);
      fs.writeFileSync('tableData.json',JSON.stringify(tableData));
      console.log(`-----PARSING DONE AND RESULTS IN tableData.json------`);
      process.exit(0);
  }
  else {
      tableData =fs.readFileSync('tableData.json','UTF8');
      tableData = convertToTable(JSON.parse(tableData));
      console.log(tableData);
  } 
}

 main();